Mirror from FlashDB https://github.com/armink/FlashDB

# Compile

> Release build: 
```bash
cmake -DCMAKE_BUILD_TYPE=Release -B build/
cmake --build build/
```

> Debug build:
```bash
cmake -DCMAKE_BUILD_TYPE=Debug -DUSE_ASAN=(ON || OFF) -B build/
cmake --build build/
```

# Usage

> Shared demo and ASAN workaround
```bash
GCC_ASAN_PRELOAD=$(gcc -print-file-name=libasan.so)
LD_PRELOAD=$GCC_ASAN_PRELOAD ./fdb_demo_shared
```
